This file describes how to create a VPC (Virtual Private Cloud) composed of public and private subnet, one EC2 (Elastic Compute Cloud) for each subnet and a NAT Gateway.  
The purpose of this lab is to be able to remote access the server in the private subnet and ping google.com. This task can only be done using an RDP (Remote Desktop Protocol) or an SSH (Secure Shell) protocol from the server in the public subnet. 

Amazon Web Service will be our cloud provider and we will used Windows Server 2012 as a server OS (Operating System).

Instructions

Creation of a VPC

-	Open your AWS management console, on the service menu, click on VPC
-	On the VPC dashboard, click on your VPCs on the left menu
-	Here you will see by default one or two predefined VPC, those are created by default by Amazon for the users who have no knowledge on how to create a VPC and want directly to create and an EC2 instance to a VPC.
Click on Create VPC.
-	In the field Name tag, enter a name for you VPC, Ex: (Class lab 1)
-	Since we want to create a private network, we have to define the network ip range, in the field IPv4 CIDR block, enter your network ip, Ex: 10.0.0.0 /16
-	Using the Amazon provided IPv6 block you can associate a blokc of IPv6 address to your network, but we will not use this option for the lab.
-	Leave the tenancy as its defaults value and click on Yes, create.
Creation of a Subnet
Our VPC will be divided into two subnets one private and one public. 
-	To create a subnet, on the left menu of the VPC Dashboard select Subnets
-	Click on create Subnet
-	In the Name tab field enter a name for your subnet, Ex: Public subnet
-	Choose the VPC we the name ��Class lab 1�� or your VPC name
-	Select an Availability Zone, this zone should be the same for your private subnet
-	Give a range of ip for your network Ex: 10.0.0.0 /24
-	Click on Yes, create
-	Do the same for your private subnet, Ex: name: Private subnet, availability zone: same as previously, network ip: 10.0.1.0 /24
Creation of a Route Tables
In AWS the difference between a private and a public subnet is that public subnet has in its route table the route of the Internet Gateway and the private the route of the NAT (Network Address Translation). 
When you created your VPC, AWS creates automatically a public route table for you.
-	on the left menu of the VPC Dashboard click on Route Tables, the route table that AWS has created for you has the name ��Class Lab 1�� under the VPC column, and under Main has Yes. On the column Name, rename it has Public-RT
-	Click on create Route Table
-	Give a name to the second Route Table Ex: Private-RT and select ��Class Lab 1�� as a VPC
-	Click Yes, create
We have now two different subnets, one private, one public. We see that under the Explicitly Associated its written 0 Subnets. We have to add to associate our subnets to their corresponding Route table.
-	Select your Public-RT and on the box below, click on Subnet Associations
-	Click on Edit
-	Select the Public subnet, and on Save
-	We do the same for the Private-RT
Now on the Explicitly Associated column, each route has 1 subnet
Creation of the Internet Gateway (IGW)
To allow our public network to be reach from outside or to have access to internet, we have to create an internet gateway and attach it to the VPC
-	To create an Internet Gateways, on the left menu of the VPC Dashboard select Internet Gateways
-	Click on Create Internet Gateway
-	Enter a name, Ex: �lab1-IGW�
-	Click on create
-	Once created, select it and on the Actions box, select Attach to VPC
-	Select the VPC �Class Lab 1�, click on save
Add the IGW to the public route table
-	Select Route Tables on the VPC Dashboard
-	Select �Public-RT�
-	On the box below select Routes
-	Click on Edit
-	Click on add another route
-	In the destination field write: 0.0.0.0 /0 and on the target �lab1�and you will see the full name of the Internet Gateway. 
-	Click on Save  
Basically, here we said that the send all the unknow traffic to the IGW
Creation of the NAT Gateway
Since our private server must not be reachable from outside but they must still have internet connection so, we have to create a NAT Gateway to allow the private host (computer or server) to have internet access but they still cannot be reach from outside the private network.
-	To create a NAT Gateways, on the left menu of the VPC Dashboard select NAT Gateways
-	Click on Create NAT Gateway
The NAT Gateway can only be created on the public subnet
-	Select the id of the public subnet. Ex: �subnet-00c53489fbaca0387�
-	Click on Create New EIP (Elastic IP Allocation ID). This will assign a static IP to your NAT Gateway
-	Click on Create a NAT Gateway
Add the NAT Gateway to the private route table
-	Select Route Tables on the VPC Dashboard
-	Select �Private-RT�
-	On the box below select Routes
-	Click on Edit
-	Click on add another route
-	In the destination field write: 0.0.0.0 /0 and on the target select �NAT�
-	Click on Save  
Basically, here we said that the send all the unknow traffic to the NAT Gateway

Creation of the Windows Server EC2 instances
Now that the private and public subnet has been created we have to add host machine.
-	To create an EC2 instances, on the service menu, click on EC2
-	 On the EC2 dashboard, click on Instances on the left menu
-	Select Launch Instance
-	Search for Windows Server 2016 Base, and click on select
-	Select your instance type, we will used the default one: �t2.micro�, and next
On Configure Instance Details, search for:
-	Network, and select �Class Lab 1�
-	 Subnet, and select �Public�
-	Leave the other as default and click next

-	On Storage leave the default setting and next
-	Click on add Tag
-	On the first field type Name
-	On the second, give a name for the server. Ex.: Public Windows Server | Bastion
-	Click Next
On Configure Security Group
-	Modify the type of default Rule, and select RDP
-	Click on add rule, on type select http, on Source select Anywhere
-	Do the same for https
-	And click Review and Launch

-	Click on launch, a box will appear for the key pair
-	Select create a new key pair, give a key pair name Ex: Windows Server Key Pair, and download
-	The EC2 instance is now created, click on view instances

-	Select the Public Windows Server Instances and click on Connect
-	 Click on Get Password, if it doesn�t work at the begin just wait and try later
-	 Browse the downloaded Key pair, and click on get password
-	Copy the display information 
Do the same process for the second EC2 instance but,
On Configure Instance Details, search for:
-	Network, and select �Class Lab 1�
-	 Subnet, and select �Private�
On Storage leave the default setting and next
-	Click on add Tag
-	On the first field type Name
-	On the second, give a name for the server. Ex.: Private Windows Server 
And finally
-	For the key pair, choose existing Key Pair and use the �Windows Server Key Pair�

Connection to the Public Windows Server
-	On the instance dashboard, select the �Public Windows Server�, click connect
-	Click on Download Remote Desktop File, Open the Remote Desktop File and click on connect
-	Insert the password and connect
-	Once connected to the public Windows Server, on the Windows menu, search for Remote Desktop Connection
-	Enter the ip address of the private server and click connect
-	Enter the username, the password and connect
-	You are now connected to the private server
-	Open a terminal and ping google.com. Command: ping google.com
-	You will see that packet are going and reach google server

End of the lab  
